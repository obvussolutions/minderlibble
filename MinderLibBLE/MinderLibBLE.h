//
//  MinderLibBLE.h
//  MinderLibBLE
//
//  Created by Eric Yuen on 10/26/16.
//  Copyright © 2016 WHI. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreBluetooth;
@import QuartzCore;

//#define DEVICE_SERVICE_UUID @"0783B03E-8535-B5A0-7140-A304D2495CB7"
//#define CHARACTERISTIC_NOTICE_UUID @"0783B03E-8535-B5A0-7140-A304D2495CB8"
//#define CHARACTERISTIC_WRITE_UUID @"0783B03E-8535-B5A0-7140-A304D2495CBA"
//#define CHARACTERISTIC_OTHER_UUID @"0783B03E-8535-B5A0-7140-A304D2495C9"

#define DEVICE_SERVICE_UUID @"6e400001-b5a3-f393-e0a9-e50e24dcca9e"
#define CHARACTERISTIC_NOTICE_UUID @"6e400003-b5a3-f393-e0a9-e50e24dcca9e"
#define CHARACTERISTIC_WRITE_UUID @"6e400002-b5a3-f393-e0a9-e50e24dcca9e"
#define CHARACTERISTIC_OTHER_UUID @"0783B03E-8535-B5A0-7140-A304D2495C9"

 
#define WRITE_COMMAND (uint8_t)119
#define STOP_COMMAND (uint8_t)0x31

#define ENABLE_ACCEL (uint8_t)97
#define DISABLE_ACCEL (uint8_t)65

#define ENABLE_GYRO (uint8_t)103
#define DISABLE_GYRO (uint8_t)71

#define ENABLE_ANGLE (uint8_t)98
#define DISABLE_ANGLE (uint8_t)66

#define ENABLE_STEP (uint8_t)115
#define DISABLE_STEP (uint8_t)83

#define ENABLE_HEARTRATE (uint8_t)104
#define DISABLE_HEARTRATE (uint8_t)72
#define SENSOR_CALIBRATION (uint8_t)99
#define ERASE_CALIBRATION (uint8_t)101
#define START_DATA (uint8_t)48
#define BLINK_LED (uint8_t)108
#define VIBRATE_DEVICE (uint8_t)76
#define VIBRATE_PATTERN_1 (uint8_t) 10


#define TIME_HEADER 0
#define ACCEL_HEADER 1
#define GYRO_HEADER 2
#define ANGLE_HEADER 3
#define COMMAND_HEADER 4
#define STEP_HEADER 7
#define HEARTRATE_HEADER 8
#define DATAGRAM_HEADER 9
#define PIC_VERSION_HEADER 10

#define INITIALIZING 0
#define CONNECTING 1
#define CONNECTED 2
#define DISCONNECTED 3
#define DISCONNECTING 4

#define POWEREDOFF 1
#define POWEREDON 2
#define UNAUTHORIZED 3
#define UNKNOWNSTATE 4
#define UNSUPPORTED 5
#define MESSAGE_LEN 20


@protocol MinderLibDelegate
- (void)updateStatus:(int)status;
- (void)updateCharge:(float)charge;
- (void)updatePosture:(float)x withY:(float)y withT:(double)t;
- (void)updateAccelWithX:(int)x withY:(int)y withZ:(int)z withT:(double)t;
- (void)updateGyroWithRoll:(int)roll withPitch:(int)pitch withYaw:(int)yaw withT:(double)t;
- (void)updateStepCount:(int)stepCount;
- (void)updateLRA:(int)wave;
- (void)updateHeartRate:(int)heartRate;
- (void)updateTemperature:(double)temperature;
- (void)updateWirelessChargerStatus:(int)status;
- (void)updateButtonStatus:(int)status;
- (void)updateDeviceList:(NSMutableArray *)devices;
- (void)updateChargingStatus:(int)status;
- (void)updateBLEStatus:(int)status;
- (void)updateDataSent:(NSMutableData *)data;
- (void)updateLEDStatus:(int)red withGreen:(int)green withBlue:(int)blue;
- (void)updateFWVersion:(NSString *)version;
- (void)updateVSysCode:(float)vsysCode;
- (void)updateRawAngle:(int)x withY:(int)y withZ:(int)z;
- (void)updateHeartRate2:(int)heartRate;
- (void)updatePicVersion:(NSString *)version;
- (void)updateRSSI:(NSNumber *)rssi;
- (void)updateDebug:(NSString *)data;
- (void)updatePICStatus:(int)status;
- (void)updateTemperatureStatus:(int)status;
- (void)updateEOCStatus:(int)status;
@end

@interface MinderLibBLE : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>
- (id) initWithDelegate:(id <MinderLibDelegate>)delegate;

@property (readonly) id <MinderLibDelegate> dataDelegate;
@property (readonly) int status;
@property (readonly) float charge;

//- (BOOL)connect;
- (BOOL)connect:(NSString *)deviceName;

- (void)disconnect;

//- (BOOL)startDataCollection;
- (BOOL)enableAccel;
- (BOOL)disableAccel;
//- (BOOL)enableGyro;
//- (BOOL)disableGyro;
- (BOOL)blinkLEDwithColor:(uint8_t)color withIntensity:(uint8_t)intensity;
- (BOOL)blinkLED:(uint8_t)color withPeriod:(short)period withDutyCycle:(short)dutyCycle;
- (BOOL)vibrateDevice;
//- (BOOL)enableAngle;
//- (BOOL)disableAngle;
//- (BOOL)enableStep;
//- (BOOL)disableStep;
- (BOOL)enableHeartRate;
- (BOOL)disableHeartRate;
//- (BOOL)calibrateSensor;
//- (BOOL)eraseCalibration;
- (BOOL)stopDevice;
- (void)setVibrationPattern:(uint8_t)pattern;
- (void)getDeviceNames;
- (void)logData:(BOOL)enable;
- (void)WriteToFile:(NSString *)str;
- (void)getRSSI;


// BLE

@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) CBPeripheral     *posturePeripheral;
@property (nonatomic, strong) CBCharacteristic *writeCharacteristic;


// Properties to hold data characteristics for the peripheral device
@property (nonatomic, strong) NSString   *connected;
@property (nonatomic, strong) NSString   *bodyData;

@end
