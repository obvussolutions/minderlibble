#Bluetooth Connection Module - MinderLibBLE


##Using MinderLibBLE


One method for linking the library is to do the following:


1. Drag the MinderLibBLE.xcodeproj into the new project’s Project Explorer to make it a subproject. If you have previously opened MinderLib be sure to close it first! This method will not work otherwise.
2. Click the project root select the project target.
    + Select Build Phases
        * Select Target Dependancies
            * Click the + and then select MinderLib from the popup
        * Select Link Bindary With Libraries
            * Click the + and then select libMinderLib.a from the popup
    + Select Build Settings
        * Make sure the project knows where MinderLib is if it is not in the project’s normal search path
            * Filter for “header search paths”
                * Enter the path to the library
        * Add -ObjC linker flag
            * Filter for “other linker flags”
                * Click the + and add -ObjC into the text box
3. Include the following Imports:

```
@import CoreBluetooth;
@import QuartzCore;
```


## Public methods




These are methods that the parent code will call to interact with the sensor.
```
-(id)initWithDelegate:(id <MinderLibDelegate>)delegate;
```	
Must set delegate with init for now.


```
-(BOOL)connect:(IOBluetoothDevice *)device;
```
Arguments: device
Return: successValue


Initiate a connection to sensor specified by device which is a IOBluetoothDevice object. This should start a new thread to handle connection of a new sensor. The function should return immediately. 


```
-(void)disconnect
```
Arguments: 
Return:


Initiate disconnection from sensor. 


```
@property (readonly) float charge;
```
Readonly property of MinderLib


Current battery level. Battery is kept track of internally so we don’t need to query the sensor and we can immediately return the current value. Note that MinderLib automatically notifies delegate of charge during any change by the required delegate method updateCharge.


```
@property (readonly) float status;
```
Readonly property of MinderLib


Current connection status. Connection status is kept track of internally so we can return value immediately. Note that MinderLib automatically notifies delegate of status during any change by the required delegate method updateStatus.


```
-(BOOL)enableAccel;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Enables accelerometer.


```
-(BOOL)disableAccel;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


disables accelerometer.


```
-(BOOL)enableGyro;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Enables gyroscope


```
-(BOOL)disableGyro;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Disables gyroscope


```
-(BOOL)enableAngle;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Enables angle sensor.


```
-(BOOL)disableAccel;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Disable angle sensor.


```
-(BOOL)blinkLED;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Blinks LED for 8 seconds


```
-(BOOL)vibrateDevice;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)


Vibrates device.


```
-(BOOL)enableStep;
-(BOOL)disableStep;
- (BOOL)calibrateSensor;
- (BOOL)eraseCalibration;
```
Arguments: 
Return: success(TRUE) or fail(FALSE)

Same as similar functions. Instructs sensor to do what the function name indicates.

##Delegate methods - MinderLibDelegate


These are methods that need to be implemented by the parent in order to handle messages from the Bluetooth Module (MinderLib). This allows the Bluetooth Module to asynchronously communicate with the parent code.


```
-(void) updateStatus:(int)status;
```
Arguments: status


Whenever the Bluetooth connection state of the sensor changes, send this update. This allows the module to notify parent when sensor has connected, is connecting, is disconnected.


```
-(void)updateCharge:(float)charge;
```
Arguments: charge


After we have received an update of the battery charge, send this update.


```
-(void)updatePosture:(float)x withY:(float)y withT:(double)t;
```
Arguments: x, y, t


Send the latest posture data along with received timestamp


```
-(void)updateAccelWithX:(int)x withY:(int)y withZ:(int)z withT:(double)t;
```
Arguments: x, y, z, t


Send the latest accelerometer data along with received timestamp


```
-(void)updateGyroWithRoll:(int)roll withPitch:(int)pitch withYaw:(int)yaw withT:(double)t;
```
Arguments: roll, pitch, yaw, t


Send the latest gyroscope data along with received timestamp


```
- (void)updateStepCount:(int)stepCount;
```
Arguments: step count

Send latest step count
